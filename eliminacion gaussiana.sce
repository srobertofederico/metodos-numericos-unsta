global AB;
AB=[
2,4,6,18;
4,5,6,24;
3,1,-2,4;
];

function MultiplicarFila(numerofila, k) //k escalar numero de filas
    global AB;
    AB(numerofila,:)=AB(numerofila,:)*(k);
endfunction

function OperacionFilaPivote(filapivote, numerofila, k) //k numero de filas
    global AB;
    AB(numerofila,:)=AB(numerofila,:)+AB(filapivote,:)*(k)
endfunction

disp(AB);
disp("Primer Pivoteo");
disp("F1->(1/2)F1");
MultiplicarFila(1,1/2);
disp(AB);
disp("F2->F2+(-4)F1");
disp("F3->F3+(-3)F1");
OperacionFilaPivote(1,2,-4);
OperacionFilaPivote(1,3,-3);
disp(AB);
disp("F2->F2+(-1/3)F1");
MultiplicarFila(2, -1/3);
disp(AB);
disp("F3->F3+(5)F1");
OperacionFilaPivote(2,3,5);
disp(AB);
disp("F3->-F3");
MultiplicarFila(3,-1);
disp(AB);

x3=AB(3,4);
x2=AB(2,4)-(AB(2,3)*x3);
x1=AB(1,4)-(AB(1,2)*x2)-(AB(1,3)*x3);
disp("x1:");
disp(x1);
disp("x2:");
disp(x2);
disp("x3:");
disp(x3);
