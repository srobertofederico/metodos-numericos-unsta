
function ans = fact_LU(A)
    n = size(A);    // Dimensiones de la matriz A
    
    // *************************************************************************
    // La matriz debe se cuadrada
    // *************************************************************************
    if( n(1) <> n(2) ) then
        error("!!!ERROR: La matriz debe ser cuadrada!!!")
    else
        n = n(1)
    end
    
    
    // *************************************************************************
    // Inicia la eliminacion Gauss sin pivoteo
    // *************************************************************************
    U = A                    // U se inicializa como una copia de A
    L = eye(n,n)             // Inicializa L como identidad
    for k = 1:n-1
        for j = k+1:n
            L(j,k) = U(j,k)/U(k,k)            // Multiplicadores
            U(j,:) = U(j,:) - L(j,k)*U(k,:)   // Fila j = Fila j - L(jk)*Fila k
        end
    end
    ans = struct("L", L, "U", U)              // Devuelve la estructura (L, U)
endfunction

A=[
2,3,2,4;
4,10,-4,0;
-3,-2,-5,-2;
-2,4,4,-7;
]
B=[4,-8,-4,-1]';
m=fact_LU(A);
disp('U')
disp(m.U)
disp('L')
disp(m.L)

y=m.L\B
x=m.U\y

disp("X:");
disp(x);
disp("Y:");
disp(y);
